# OpenStack CLI Container Image

```sh
podman run --rm -i \
    --security-opt label=disable \
    -v .:/openstack \
    registry.gitlab.com/abineo/infra/openstack-cli \
    flavor list
```
